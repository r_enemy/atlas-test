import { Coin } from '../../core/models/coin';

export class CoinFixture {
  static getOne(): Coin {
    return new Coin({
      type: Coin.Type.BTC,
      value: 112.62992,
    });
  }
}
