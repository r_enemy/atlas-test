import { Profit } from '../../core/models/profit';
import { CoinFixture } from './coin-fixture';


export class ProfitFixture {
  static getOne(): Profit {
    return new Profit({
      date: new Date('2018-02-19 09:07:42'),
      profit: CoinFixture.getOne(),
      profitPercentage: 38,
    });
  }
}
