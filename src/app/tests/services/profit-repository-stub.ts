import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { Profit } from '../../core/models/profit';

export class ProfitRepositoryStub {

  getProfitRecords$ = new Subject<any>();

  getProfitRecords(id: string): Observable<Profit[]> {
    return this.getProfitRecords$.asObservable();
  }
}
