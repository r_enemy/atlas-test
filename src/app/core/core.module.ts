import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { UiModule } from './ui/ui.module';
import { CommonModule } from '../common/common.module';


@NgModule({
  imports: [
    UiModule,
    CommonModule,
    HttpClientModule,
  ],
  declarations: [],
  providers: [
  ],
  exports: [
    UiModule,
    CommonModule,
  ],
})
export class CoreModule { }
