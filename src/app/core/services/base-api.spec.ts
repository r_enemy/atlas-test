import { BaseApi } from './base-api';

describe('BaseApi', () => {
  class Service extends BaseApi {
  }

  const service = new Service();

  it('creates a full url', () => {
      const url = service.createUrl(
        '/resource/:id/items/:itemId',
        {
          id: '123a',
          itemId: '456b',
        }
      );

      expect(url).toEqual('http://localhost:8080/resource/123a/items/456b');
    },
  );
});

