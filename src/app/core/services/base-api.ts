import { environment } from '../../../environments/environment';

const apiUrl =  environment.api.url;

export abstract class BaseApi {

  createUrl(resource: string, params = {}): string {
    let _resource = this.replaceParams(resource, params);
    _resource = this.normalizeResource(_resource);

    return `${apiUrl}/${_resource}`;
  }

  replaceParams(resource: string, params?: Object): string {
    // tslint:disable-next-line
    return Object.entries(params).reduce(
      (acc, [key, value]) => acc.replace(`:${key}`, value),
      resource,
    );
  }

  normalizeResource(resource: string): string {
    return resource.replace(/^\/+/, '');
  }

}
