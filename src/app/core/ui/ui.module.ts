import { NgModule } from '@angular/core';


import { CommonModule } from '../../common/common.module';
import { InnerPageComponent } from './inner-page/inner-page.component';
import { MainSidebarComponent } from './inner-page/main-sidebar/main-sidebar.component';
import { InnerPageHeaderComponent } from './inner-page/inner-page-header/inner-page-header.component';
import { InnerPageFooterComponent } from './inner-page/inner-page-footer/inner-page-footer.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    InnerPageComponent,
    MainSidebarComponent,
    InnerPageHeaderComponent,
    InnerPageFooterComponent,
  ],
  exports: [
    CommonModule,
    InnerPageComponent,
  ]
})
export class UiModule { }
