import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerPageHeaderComponent } from './inner-page-header.component';
import { CommonModule } from '../../../../common/common.module';

describe('InnerPageHeaderComponent', () => {
  let component: InnerPageHeaderComponent;
  let fixture: ComponentFixture<InnerPageHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      declarations: [ InnerPageHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerPageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
