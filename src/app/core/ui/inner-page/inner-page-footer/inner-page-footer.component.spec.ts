import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerPageFooterComponent } from './inner-page-footer.component';
import { CommonModule } from '../../../../common/common.module';

describe('InnerPageFooterComponent', () => {
  let component: InnerPageFooterComponent;
  let fixture: ComponentFixture<InnerPageFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      declarations: [InnerPageFooterComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerPageFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
