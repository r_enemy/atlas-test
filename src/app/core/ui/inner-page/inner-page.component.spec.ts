import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerPageComponent } from './inner-page.component';
import { UiModule } from '../ui.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('InnerPageComponent', () => {
  let component: InnerPageComponent;
  let fixture: ComponentFixture<InnerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiModule, RouterTestingModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
