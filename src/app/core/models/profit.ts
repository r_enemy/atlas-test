import { Coin } from './coin';

export class Profit {
  date: Date;
  profit: Coin;
  profitPercentage: number;

  constructor(
    profit: {
      date: Date;
      profit: Coin;
      profitPercentage: number;
    },
  ) {
    this.date = profit.date;
    this.profit = profit.profit;
    this.profitPercentage = profit.profitPercentage;
  }

  calcBalance(): Coin {
    const balance =
      this.profit.value / (this.profitPercentage / 100) + this.profit.value;
    return new Coin({ type: this.profit.type, value: balance });
  }
}
