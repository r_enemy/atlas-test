enum CoinType {
  BTC = 'BTC',
}

export class Coin {
  static Type = CoinType;

  private _type: CoinType;
  private _value: number;

  constructor({
    type = Coin.Type.BTC,
    value = 0,
  }: {
    type: CoinType;
    value: number;
  }) {
    this._type = type;
    this._value = +value;
  }

  get type(): CoinType {
    return this._type;
  }

  get value(): number {
    return this._value;
  }
}
