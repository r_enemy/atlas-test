import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { DateComponent } from './date.component';

describe('DateComponent', () => {
  let component: DateComponent;
  let fixture: ComponentFixture<DateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('renders date successfully', () => {
    component.date = new Date('2018-02-19 09:07:02-000');
    fixture.detectChanges();

    const renderedData = fixture.debugElement
      .query(By.css('span'))
      .nativeElement.innerText.trim();

    expect(/19\/02\/2018.*09:07/.test(renderedData)).toBeTruthy();
  });
});
