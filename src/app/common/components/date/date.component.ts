import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  @Input () date: Date = new Date(0);

  constructor() { }

  ngOnInit() {
  }

  private normalizeNumber(num: number): string {
    // tslint:disable-next-line
    return num.toString().padStart(2, '0');
  }

  getDate(): string {
    const year = this.date.getUTCFullYear();
    const month = this.normalizeNumber(this.date.getUTCMonth() + 1);
    const day = this.normalizeNumber(this.date.getUTCDate());
    return `${day}/${month}/${year}`;
  }

  getTime(): string {
    const hour = this.normalizeNumber(this.date.getUTCHours());
    const minute = this.normalizeNumber(this.date.getUTCMinutes());
    return `${hour}:${minute}`;
  }

}
