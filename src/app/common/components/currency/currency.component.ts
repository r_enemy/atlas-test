import { Component, Input, OnInit } from '@angular/core';


import { Coin } from '../../../core/models/coin';


@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {
  @Input() coin: Coin = new Coin({ type: Coin.Type.BTC, value: 0 });

  constructor() { }

  ngOnInit() {
  }

  normalizeValue(): string {
    return this.coin.value.toFixed(8);
  }

}
