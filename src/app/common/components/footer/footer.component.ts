import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: '<div class="footer"><ng-content></ng-content></div>',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
