import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar-button',
  templateUrl: './sidebar-button.component.html',
  styleUrls: ['./sidebar-button.component.css']
})
export class SidebarButtonComponent implements OnInit {
  @Input() href = '';
  @Input() iconSrc = '';
  @Input() iconAlt = '';
  @Input() label = '';

  constructor() { }

  ngOnInit() {
  }

}
