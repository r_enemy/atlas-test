import { NgModule } from '@angular/core';
import { CommonModule as NgCommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


import { PageComponent } from './components/page/page.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SidebarButtonComponent } from './components/sidebar-button/sidebar-button.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BoxComponent } from './components/box/box.component';
import { CurrencyComponent } from './components/currency/currency.component';
import { DateComponent } from './components/date/date.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';


@NgModule({
  imports: [
    NgCommonModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [
    PageComponent,
    SidebarComponent,
    SidebarButtonComponent,
    HeaderComponent,
    FooterComponent,
    BoxComponent,
    CurrencyComponent,
    DateComponent,
    SearchBarComponent,
  ],
  exports: [
    NgCommonModule,
    PageComponent,
    SidebarComponent,
    SidebarButtonComponent,
    HeaderComponent,
    FooterComponent,
    BoxComponent,
    CurrencyComponent,
    DateComponent,
    SearchBarComponent
  ]
})
export class CommonModule { }
