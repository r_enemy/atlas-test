import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ProfitPageComponent } from './pages/profit-page/profit-page.component';


const routes: Routes = [
  { path: '', component: ProfitPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfitRoutingModule { }
