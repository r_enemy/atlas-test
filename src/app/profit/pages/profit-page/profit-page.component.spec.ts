import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { ProfitPageComponent } from './profit-page.component';
import { ProfitModule } from '../../profit.module';
import { ProfitRepositoryService } from '../../services/profit-repository.service';
import { ProfitFixture } from '../../../tests/fixture/profit-fixture';
import { ProfitRepositoryStub } from '../../../tests/services/profit-repository-stub';

describe('ProfitPageComponent', () => {
  let component: ProfitPageComponent;
  let fixture: ComponentFixture<ProfitPageComponent>;
  let profitRepositoty = new ProfitRepositoryStub();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ProfitModule, RouterTestingModule],
      providers: [
        { provide: ProfitRepositoryService, useValue: profitRepositoty },
      ],
    }).compileComponents();

    profitRepositoty = TestBed.get(ProfitRepositoryService);
    spyOn(profitRepositoty, 'getProfitRecords').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitPageComponent);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

    expect(profitRepositoty.getProfitRecords).toHaveBeenCalledWith(
      '5b2c010d300000100023487a',
    );
  });

  it('renders records', () => {
    const profitsMock = [ProfitFixture.getOne(), ProfitFixture.getOne()];

    profitRepositoty.getProfitRecords$.next(profitsMock);

    fixture.detectChanges();

    const profitRows = fixture.debugElement.queryAll(By.css('[profitRow]'));
    expect(profitRows.length).toEqual(2);

    const profitCells = profitRows[0].queryAll(By.css('td'));
    expect(profitCells[0].nativeElement.innerText.trim()).toMatch(
      /19\/02\/2018.*12:07/,
    );
    expect(profitCells[1].nativeElement.innerText.trim()).toEqual('BTC');
    expect(profitCells[2].nativeElement.innerText.trim()).toEqual(
      '112.62992000 BTC',
    );
    expect(profitCells[3].nativeElement.innerText.trim()).toEqual('38%');
  });

  it('renders loading placeholder', () => {
    expect(fixture.debugElement.query(By.css('[loading]'))).toBeTruthy();
  });
});
