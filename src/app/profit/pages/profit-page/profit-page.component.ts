import { Component, OnInit } from '@angular/core';
import { ProfitRepositoryService } from '../../services/profit-repository.service';
import { Profit } from '../../../core/models';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Component({
  selector: 'app-profit-page',
  templateUrl: './profit-page.component.html',
  styleUrls: ['./profit-page.component.css'],
})
export class ProfitPageComponent implements OnInit {
  profits$: Observable<Profit[]>;

  constructor(private repository: ProfitRepositoryService) {}

  ngOnInit() {
    this.profits$ = this.repository.getProfitRecords('5b2c010d300000100023487a');
  }

}
