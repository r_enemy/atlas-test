import { NgModule } from '@angular/core';

import { ProfitRoutingModule } from './profit-routing.module';
import { CoreModule } from '../core/core.module';
import { ProfitPageComponent } from './pages/profit-page/profit-page.component';
import { ProfitRepositoryService } from './services/profit-repository.service';
import { ProfitParserService } from './services/profit-parser.service';
import { ProfitFilterService } from './services/profit-filter.service';

@NgModule({
  imports: [CoreModule, ProfitRoutingModule],
  declarations: [ProfitPageComponent],
  providers: [
    ProfitRepositoryService,
    ProfitParserService,
    ProfitFilterService,
  ],
})
export class ProfitModule {}
