import { Injectable } from '@angular/core';


import { Profit, Coin } from '../../core/models';
import { ProfitDTO } from './profit-dto';


@Injectable()
export class ProfitParserService {
  constructor() {}

  parseSingle = (profit: ProfitDTO): Profit => {
    return new Profit({
      date: new Date(profit.dateMoviment),
      profit: new Coin({ type: Coin.Type.BTC, value: +profit.proft }),
      profitPercentage: profit.profitPercentage,
    });
  };

  parseMultiple = (profits: ProfitDTO[]): Profit[] => {
    return profits.map(this.parseSingle);
  };
}
