import { TestBed, inject, async } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';

import { environment } from '../../../environments/environment';
import { Profit } from '../../core/models';
import { ProfitRepositoryService } from './profit-repository.service';
import { ProfitModule } from '../profit.module';

describe('ProfitRepositoryService', () => {
  let httpClient: HttpClient;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ProfitModule, HttpClientTestingModule],
    });

    httpClient = TestBed.get(HttpClient);
    httpController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should be created', inject(
    [ProfitRepositoryService],
    (service: ProfitRepositoryService) => {
      expect(service).toBeTruthy();
    },
  ));

  it('gets all profit records', inject(
    [ProfitRepositoryService],
    (service: ProfitRepositoryService) => {
      const mockData = [
        {
          dateMoviment: '2018-10-15 21:51:03',
          coin: 'BTC',
          proft: '0.668255',
          profitPercentage: 38,
        },
      ];

      service
        .getProfitRecords('12345')
        .subscribe(data => {
          expect(data[0]).toEqual(jasmine.any(Profit));
        });

      const req = httpController.expectOne(`${environment.api.url}/12345`);

      req.flush(mockData);

      expect(req.request.method).toEqual('GET');
    },
  ));
});
