export interface ProfitDTO {
  dateMoviment: string;
  coin: string;
  proft: string;
  profitPercentage: number;
}
