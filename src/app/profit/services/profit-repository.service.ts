import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Profit } from '../../core/models';
import { BaseApi } from '../../core/services/base-api';
import { ProfitParserService } from './profit-parser.service';
import { ProfitFilterService } from './profit-filter.service';
import { ProfitDTO } from './profit-dto';

@Injectable()
export class ProfitRepositoryService extends BaseApi {
  constructor(
    private http: HttpClient,
    private parser: ProfitParserService,
    private filter: ProfitFilterService,
  ) {
    super();
  }

  getProfitRecords(id: string): Observable<Profit[]> {
    const url = this.createUrl(id);
    return this.http
      .get<ProfitDTO[]>(url)
      .map(data => {
        const parsedData = this.parser.parseMultiple(data);
        return this.filter.filterLastMonth(parsedData);
      }) as Observable<Profit[]> ;
  }
}
