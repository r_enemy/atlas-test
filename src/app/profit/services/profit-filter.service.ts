import { Injectable } from '@angular/core';


import { Profit } from '../../core/models';


@Injectable()
export class ProfitFilterService {

  constructor() { }

  filterLastMonth = (profits: Profit[]): Profit[] => {
    const filteredProfits = profits.filter(this.isDateValid);
    filteredProfits.sort(this.sortDesc);
    return filteredProfits;
  };

  private sortDesc = (a: Profit, b: Profit): number => {
    return  b.date.getTime() - a.date.getTime();
  };

  private isDateValid = (profit: Profit): boolean => {
    const now = new Date();
    const thirtyDaysAgo = new Date(now.getTime() - 30 * 24 * 3600 * 1000);
    return profit.date >= thirtyDaysAgo && profit.date <= now ;
  };
}
