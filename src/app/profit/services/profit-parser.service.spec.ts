import { TestBed, inject } from '@angular/core/testing';

import { ProfitParserService } from './profit-parser.service';
import { Coin, Profit } from '../../core/models';

describe('ProfitParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfitParserService],
    });
  });

  it('should be created', inject(
    [ProfitParserService],
    (service: ProfitParserService) => {
      expect(service).toBeTruthy();
    },
  ));

  it('creates a Profit object', () => {
    const service = new ProfitParserService();
    expect(service).toBeTruthy();

    const profit = service.parseSingle({
      dateMoviment: '2018-11-07 21:51:03',
      coin: 'BTC',
      proft: '0.668255',
      profitPercentage: 38,
    });

    expect(profit).toEqual(
      new Profit({
        date: new Date('2018-11-07 21:51:03'),
        profit: new Coin({ type: Coin.Type.BTC, value: 0.668255 }),
        profitPercentage: 38,
      }),
    );
  });

  it('creates multiple Profits', () => {
    const service = new ProfitParserService();
    expect(service).toBeTruthy();

    const profit = service.parseMultiple([
      {
        dateMoviment: '2018-11-07 21:51:03',
        coin: 'BTC',
        proft: '0.668255',
        profitPercentage: 38,
      },
    ]);

    expect(profit).toEqual([
      new Profit({
        date: new Date('2018-11-07 21:51:03'),
        profit: new Coin({ type: Coin.Type.BTC, value: 0.668255 }),
        profitPercentage: 38,
      }),
    ]);
  });
});
